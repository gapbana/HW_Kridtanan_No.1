'use strict'
let fs = require('fs');

async function readDB() {
    try {
        const strData2 = await jsonData();
        return  JSON.parse(strData2)

    } catch (error) {
        console.error(error);
    }
}

function jsonData() {
    return new Promise(function (resolve, reject) {
        fs.readFile('homework2_1.json', 'utf8', function (err, json) {
            if (err) {
                reject(err);
                return json
            }
            resolve(json);
        });
    });
}

module.exports = readDB;