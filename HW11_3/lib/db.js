const mysql = require('mysql2/promise');

// create the connection
const pool = mysql.createPool({
    connectionLimit: 10,
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'codecamp2'
});

module.exports = pool;