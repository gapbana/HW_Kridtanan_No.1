const Koa = require('koa');
const Router = require('koa-router');
const serve = require('koa-static');
const path = require('path'); //import library path
const render = require('koa-ejs');  //import koa ejs
const pool = require('./lib/db.js');
const myQuery = require('./models/from_database');
const jsonData = require('./models/from_file');
const db = require('./lib/db.js');
var now = new Date();

//สร้าง path ใหม่ = copy route

const app = new Koa();
const router = new Router();

render(app, {    //การ setting ejs
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
})

/* async function readDB() {
    try {
        const strData1 = await myQuery();
        const strData2 = await jsonData();
        return strData1, strData2

    } catch (error) {
        console.error(error);
    }
} */


function additionalData(ctx,next) {
//    return "\n additionalData :  {\n"  +"   userId: 1,\n"
//    + "   dateTime: "+ now.getFullYear()+"-" + (now.getMonth()+1) +"-"+ now.getDate() 
//    + "  " + now.getHours() +":" + now.getMinutes()+":" + now.getSeconds() +"\n}"; 
    return {
        additionalData: {
            userId: 1,
            dateTime: `${now.getFullYear()}-${(now.getMonth()+1)}-${now.getDate()}  ${now.getHours()}-${now.getMinutes()}-${now.getSeconds()}`
        }
    }
}


 
router.get('/from_file', async ctx => {
    const result = await jsonData();
    result.push(additionalData())
    ctx.body = result;
})

router.get('/from_database', async ctx => {
    const result = await myQuery();
    result.push(additionalData())
    ctx.body = result;
})

app.use(serve(path.join(__dirname, 'public')));
app.use(router.routes());
app.use(router.allowedMethods());

app.listen(3010);
