const Koa = require('koa');
const Router = require('koa-router');
const serve = require('koa-static');
const path = require('path'); //import library path
const render = require('koa-ejs')  //import koa ejs
/* const mysql = require('mysql2/promise');  // get the client */
const mysql = require('mysql2/promise')



//สร้าง path ใหม่ = copy route

const app = new Koa();
const router = new Router();

render(app, {    //การ setting ejs
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
})

// create the connection
const pool = mysql.createPool({
    connectionLimit: 10,
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'hw13'
});


async function countMoneyQuery() {
    let [rows] = await pool.query('select enrolls.course_id , courses.name , courses.price from enrolls JOIN courses ON enrolls.course_id = courses.id UNION select"","รวม",(SELECT SUM(price) FROM  enrolls INNER JOIN courses ON enrolls.course_id=courses.id)');
    return rows;
}

async function moneyPerStudentQuery() {
    let [rows] = await pool.query('SELECT enrolls.student_id id,students.name full_name ,enrolls.course_id,courses.name course_name ,sum(courses.price) sum_price FROM enrolls JOIN courses ON enrolls.course_id=courses.id JOIN students ON enrolls.student_id = students.id GROUP by enrolls.student_id');  
    return rows;
}

async function readDB() {
    try {
        const strData = await countMoneyQuery();
        return strData

    } catch (error) {
        console.error(error);
    }
}

 async function readDB2() {
    try {
        const strData = await moneyPerStudentQuery();
        return strData

    } catch (error) {
        console.error(error);
    }
} 


router.get('/', async ctx => {
    const result = await readDB();
    const result2 = await readDB2();
    let obj = {};
    obj.t1 = result;
    obj.t2 = result2;
    await ctx.render('table', obj)
  console.log(obj);
    console.log(typeof (obj));

})

console.log()
app.use(serve(path.join(__dirname, 'public')));
app.use(router.routes());
app.use(router.allowedMethods());

app.listen(3010);
