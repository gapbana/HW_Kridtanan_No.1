const Router = require('koa-router')
const koaBody = require('koa-body')
const uuidv4 = require('uuid/v4') // ลง ด้วยการ npm install uuid
const fs = require('fs-extra') // ลง ด้วยการ npm install fs-extra


const create = require('./create')
const detail = require('./detail')
const comment = require('./comment')
const like = require('./like')

const router = new Router()

router.get('/create', create.getHandler)
router.post('/create', create.postHandler)
router.get('/pikka/:id', detail.getHandler)
router.post('/pikka/:id/comment', comment.postHandler)
router.post('/pikka/:id/like', like.postHandler)

const allowFileType = {
    'image/png': true,
    'image/jpeg': true
   }
   

module.exports = router.routes()
