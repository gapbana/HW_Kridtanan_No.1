const Koa = require('koa')
const koaBody = require('koa-body')
const serve = require('koa-static')
const Router = require('koa-router');
const path = require('path'); //import library path
const render = require('koa-ejs')  //import koa ejs
const mysql = require('mysql2/promise')
const fs = require('fs-extra')

const router = new Router();


const allowFileType = {
	'image/png': true,
	'image/jpeg': true
}

const pictureDir = path.join(__dirname, 'public', 'images')



const getHandler = async ctx => {
	try {
		if (!allowFileType[ctx.request.files.photo.type]) {
			throw new Error('file type not allow')
		}
		console.log(ctx.request.body.caption)
		console.log(ctx.request.body.detail)
		console.log(ctx.request.files.photo.name)
		console.log(ctx.request.files.photo.path)
		const fileName = uuidv4()
		await fs.rename(ctx.request.files.photo.path, path.join(pictureDir, fileName))
		ctx.status = 303
		ctx.redirect('/')
	} catch (e) {
		// handle error here
		ctx.status = 400
		ctx.body = e.message
		fs.remove(ctx.request.files.photo.path)
	}
}



const postHandler = (ctx) => {

}

router.post('/upload', koaBody({ multipart: true }), getHandler)




module.exports = {
	getHandler,
	postHandler
}
