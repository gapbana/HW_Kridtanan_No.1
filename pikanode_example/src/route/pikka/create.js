const { pictures } = require('../../repository')

const uuidv4 = require('uuid/v4') // ลง ด้วยการ npm install uuid
const fs = require('fs-extra') // ลง ด้วยการ npm install fs-extra
const path = require('path')
const Koa = require('koa')
const Router = require('koa-router');
const render = require('koa-ejs')  //import koa ejs


const app = new Koa()
const router = new Router()

const pictureDir = path.join(process.cwd(), 'public', 'images')
/* console.log(pictureDir) */


const getHandler = async (ctx) => {
	await ctx.render('create')

}

const allowFileType = {
	'image/png': true,
	'image/jpeg': true
}



const postHandler = async (ctx) => {
	try {
		// check allow file type
		if (!allowFileType[ctx.request.files.photo.type]) {
			throw new Error('file type not allow')
		}
		// form datas
		console.log(ctx.request.body.caption)
		console.log(ctx.request.body.detail)
		// file datas
		console.log(ctx.request.files.photo.name)
		console.log(ctx.request.files.photo.path)

		const { caption, ictures } = ctx.request.body
		const fileName = uuidv4() // generate uuid for file name
		// move uploaded file from temp dir to destination
		await fs.rename(ctx.request.files.photo.path, path.join(pictureDir, fileName ))
		const picId = await pictures.pictures(caption,fileName ,1)
		ctx.redirect('/')  //change page
	} catch (err) {
		// handle error here
		ctx.status = 400
		ctx.body = err.message
		// remove uploaded temporary file when the error occurs
		fs.remove(ctx.request.files.photo.path)
	}
}


module.exports = {
	getHandler,
	postHandler
}
