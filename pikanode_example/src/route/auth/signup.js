const { user } = require('../../repository')


const getHandler = async (ctx) => {
	await ctx.render('signup')
}

const postHandler = async (ctx) => {
	const { email, password } = ctx.request.body
	// TODO: validate email, password
	const userId = await user.register(email, password)
	
	// TODO: handle user id ?
}

module.exports = {
	getHandler,
	postHandler
}
