const user = require('./user')
const pictures = require('./pictures')
const allPic = require('./allPic')

module.exports = {
	user,
	pictures,
	allPic
}
