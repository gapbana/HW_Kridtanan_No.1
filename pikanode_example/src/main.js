const Koa = require('koa')
const koaBody = require('koa-body')
const serve = require('koa-static')
const Router = require('koa-router');
const path = require('path'); //import library path
const render = require('koa-ejs')  //import koa ejs
const mysql = require('mysql2/promise')

const app = new Koa()
const router = new Router();

render(app, {    //การ setting ejs
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
})

const stripPrefix = async (ctx, next) => {
	if (!ctx.path.startsWith('/-')) {
		ctx.status = 404
		return
	}

	ctx.path = ctx.path.slice(2)
	await next()
}
/* 
const signin = require('../src/route/auth/signin')
router.get('',signin) */

app.use(koaBody({ multipart: true }))
app.use(require('./route'))

app.use(stripPrefix)
app.use(serve('public'))
app.use(router.allowedMethods());

app.listen(8000)
