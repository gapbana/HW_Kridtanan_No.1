

//รับค่า id เพื่อแสดง JSON API ของ course id ที่ระบุ จากตาราง courses
router.get('/course/find_by_id/:id', async (ctx, next) => {
    const [rows] = await pool.query('SELECT * FROM `courses` WHERE courses.id = ?', [ctx.params.id]);
    ctx.body = rows;
    await next();
});

//รับค่า price เพื่อแสดง JSON ของ price ที่ระบุจากตาราง courses
router.get('/course/find_by_price/:price', async (ctx, next) => {
    const [rows] = await pool.query('SELECT * FROM `courses` WHERE courses.price = ?', [ctx.params.price]);
    ctx.body = rows;
    await next();
});
