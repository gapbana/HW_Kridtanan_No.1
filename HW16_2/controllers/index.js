
    async function createEntity(row) {
    if (!row.id)
        return [];

    return {
        id: row.id,
        firstname: row.firstname,
        lastname: row.lastname,
        salary: row.salary,
        money: row.money,
        jsonData: JSON.parse(row.json_data)
    }
}
