'use strict'

const Koa = require('koa')
const Router = require('koa-router')
const serve = require('koa-static')
const path = require('path')
const render = require('koa-ejs')
const fs = require('fs');

const app = new Koa()
const router = new Router()

render(app, {
  root: path.join(__dirname, 'views'),
  layout: 'template',
  viewExt: 'ejs',
  cache: false
})

function jsonData() {
  return new Promise(function (resolve, reject) {
    fs.readFile('homework2_1.json', 'utf8', function (err, DataLeg) {
      if (err) {
        reject(err);
        return
      }
      resolve(DataLeg);
    });
  });
}

async function readFiles() {
  try {
    const strData = await jsonData();
    return strData

  } catch (error) {
    console.error(error);
  }
}
let results = readFiles();

router.get('/', async ctx => {
  const result = await readFiles();
  let obj = {};
  obj.users = JSON.parse(result)
  console.log(obj);
  await ctx.render('index', obj)
})

app.use(serve(path.join(__dirname, "public")))
app.use(router.routes())
app.use(router.allowedMethods())
app.listen(3003)
