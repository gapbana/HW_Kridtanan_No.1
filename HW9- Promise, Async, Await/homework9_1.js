'use strict'

let fs = require('fs');

async function readFile() {
    try {
        let dataHead = await readFileHead();
        //console.log(dataHead)
        let dataBody = await readFileBody();
        //console.log(dataBody)
        let dataLeg = await readFileLeg();
        //console.log(dataLeg)
        let dataFeet = await readFileFeet();
        //console.log(dataFeet)
        let dataRobot = await writeFileRobot();
        
        fs.appendFile('robot.txt', dataHead + "\n" +dataBody+"\n"+dataLeg+"\n"+dataFeet, 'utf8', function (err) {
        });
        
    } catch (error) {
        console.error(error);
    }
}

function readFileHead(){
    return new Promise(function(resolve, reject) {
        fs.readFile('head.txt', 'utf8', function (err, dataHead) {
          if (err)
            reject(err);
          else
            resolve(dataHead);
        });
      });
    
}

function readFileBody(){
    return new Promise(function(resolve, reject) {
        fs.readFile('body.txt', 'utf8', function (err, dataBody) {
          if (err)
            reject(err);
          else
            resolve(dataBody);
        });
      });
}

function readFileLeg(){
    return new Promise(function(resolve, reject) {
        fs.readFile('leg.txt', 'utf8', function (err, dataLeg) {
          if (err)
            reject(err);
          else
            resolve(dataLeg);
        });
      });
}

function readFileFeet(){
    return new Promise(function(resolve, reject) {
        fs.readFile('feet.txt', 'utf8', function (err, dataLeg) {
          if (err)
            reject(err);
          else
            resolve(dataLeg);
        });
      });
}

function writeFileRobot(){
    return new Promise(function(resolve, reject) {
        fs.writeFile('robot.txt', '', 'utf8', function (err) {
          if (err)
            reject(err);
          else
            resolve();
            
        });
      });
}

readFile();


