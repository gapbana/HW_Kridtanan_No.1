let arr = [1,2,3,4,5,6,7,8,9,10];


function fillArr(arr) {
  return arr%2 ===0;
}

function multiply1000(arr) {
  return arr*1000;
}


//แบบปกติ
/* const result = arr.filter(arrs => {
  return arrs%2 ===0
});


const resultMap = result.map(x => x * 1000);
console.log(resultMap); */


//chain

const result = arr
  .filter(fillArr)
  .map(multiply1000)
console.log(result);