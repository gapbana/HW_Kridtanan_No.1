const Koa = require('koa');
const Router = require('koa-router');
const serve = require('koa-static');
const path = require('path'); //import library path
const render = require('koa-ejs')  //import koa ejs
/* const mysql = require('mysql2/promise');  // get the client */
const mysql = require('mysql2/promise')
const koaBody = require('koa-body')




//สร้าง path ใหม่ = copy route

const app = new Koa();
const router = new Router();

render(app, {    //การ setting ejs
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
})

// create the connection
const pool = mysql.createPool({
    connectionLimit: 10,
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'design_pattern'
});

//แสดง JSON API ของ instuctor ทั้งตาราง
async function findAllIns() {
    let [rows] = await pool.query('SELECT * FROM `instructors`');
    return rows;
}

//รับค่า id เพื่อแสดง JSON API ของ instructor id ที่ระบุ จากตาราง instructors
async function findIns() {
    let [rows] = await pool.query('SELECT * FROM `instructors` WHERE instructors.id = ?', [ctx.params.id]);
    return rows;
}

//รับค่า id เพื่อแสดง JSON API ของ course id ที่ระบุ จากตาราง courses
async function findCourse() {
    let [rows] = await pool.query('SELECT * FROM `courses` WHERE courses.id= ?', [ctx.params.id]);
    return rows;
}

//รับค่า price เพื่อแสดง JSON ของ price ที่ระบุจากตาราง courses
async function showPrice() {
    let [rows] = await pool.query('SELECT * FROM `instructors` WHERE instructors.id = ?', [ctx.params.id]);
    return rows;
}

/* async function readDB() {
    try {
        const strData = await countMoneyQuery();
        return strData

    } catch (error) {
        console.error(error);
    }
}

 async function readDB2() {
    try {
        const strData = await moneyPerStudentQuery();
        return strData

    } catch (error) {
        console.error(error);
    }
} 


router.get('/', async ctx => {
    const result = await readDB();
    const result2 = await readDB2();
    let obj = {};
    obj.t1 = result;
    obj.t2 = result2;
    await ctx.render('table', obj)
  console.log(obj);
    console.log(typeof (obj));

}) */

//แสดง JSON API ของ instuctor ทั้งตาราง
router.get('/instructor/find_all', async (ctx, next) => {
    const [rows] = await pool.query('SELECT * FROM `instructors`');
    ctx.body = rows;
    await next();
});

//รับค่า id เพื่อแสดง JSON API ของ instructor id ที่ระบุ จากตาราง instructors
router.get('/instructor/find_by_id/:id', async (ctx, next) => {
    const [rows] = await pool.query('SELECT * FROM `instructors` WHERE instructors.id = ?', [ctx.params.id]);
    ctx.body = rows;
    await next();
});

//รับค่า id เพื่อแสดง JSON API ของ course id ที่ระบุ จากตาราง courses
router.get('/course/find_by_id/:id', async (ctx, next) => {
    const [rows] = await pool.query('SELECT * FROM `courses` WHERE courses.id = ?', [ctx.params.id]);
    ctx.body = rows;
    await next();
});

//รับค่า price เพื่อแสดง JSON ของ price ที่ระบุจากตาราง courses
router.get('/course/find_by_price/:price', async (ctx, next) => {
    const [rows] = await pool.query('SELECT * FROM `courses` WHERE courses.price = ?', [ctx.params.price]);
    ctx.body = rows;
    await next();
});

async function signinGetHandler (ctx) {
    await ctx.render('signin')
   }
   
   function signinPostHandler (ctx) {
    console.log('username: ', ctx.request.body.username)
    console.log('password: ', ctx.request.body.password)
    ctx.status = 303
    ctx.redirect('/signin')
   }
   


app.use(ctx => {
    console.log(ctx.request.body)
})

/* router.get('/signin', signinGetHandler) */
router.post('/signin', signinPostHandler)


app.use(koaBody())
app.use(serve(path.join(__dirname, 'public')));
app.use(router.routes());
app.use(router.allowedMethods());

app.listen(3011);
