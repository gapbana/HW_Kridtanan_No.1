const Koa = require('koa');
const Router = require('koa-router');
const render = require('koa-ejs');
const serve = require('koa-static');
const path = require('path'); //import library path


const app = new Koa();
const router = new Router();


//การ Setting render ejs
render(app, {                      
    root: path.join(__dirname, 'views'),
    layout: 'template',  //ไฟล์โครงสร้างของเรา มาจาก template.ejs
    viewExt: 'ejs',
    cache: false
})

// render หน้าหลัก หมายถึงถ้า เข้าที่ http://localhost:3003/ แล้วเข้ามาที่หน้า index
router.get('/', async ctx => {
    await ctx.render('index')
})


// render หน้าSkill หมายถึงถ้า เข้าที่ http://localhost:3003/skill แล้วเข้ามาที่หน้า skill
router.get('/skill', async ctx => {  //ห้ามลืม path
    await ctx.render('skill')
})


// render หน้าContact me หมายถึงถ้า เข้าที่ http://localhost:3003/Contact แล้วเข้ามาที่หน้า Contact
router.get('/contact', async ctx => {  //ห้ามลืม path
    await ctx.render('contact')
})


// render หน้าPortflolio หมายถึงถ้า เข้าที่ http://localhost:3003/Portflolio  แล้วเข้ามาที่หน้า Portflolio 
router.get('/portflolio', async ctx => {  //ห้ามลืม path
    await ctx.render('portflolio')
})




//เรียกใช้งาน
app.use(serve(path.join(__dirname, 'public')));
app.use(router.routes());
app.use(router.allowedMethods());

//ประกาศ http://localhost:3003/ ให้ใช้งานได้
app.listen(3005);