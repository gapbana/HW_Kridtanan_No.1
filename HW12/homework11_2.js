const Koa = require('koa');
const Router = require('koa-router');
const serve = require('koa-static');
const path = require('path'); //import library path
const render = require('koa-ejs')  //import koa ejs
/* const mysql = require('mysql2/promise');  // get the client */
const mysql = require('mysql2/promise')



//สร้าง path ใหม่ = copy route

const app = new Koa();
const router = new Router();

render(app, {    //การ setting ejs
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
})

// create the connection
const pool = mysql.createPool({
    connectionLimit: 10,
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'codecamp2'
});


async function myQuery() {
    let [rows] = await pool.query('SELECT * FROM user');
    /* console.log('The solution is: ', rows); */
    return rows;
}

async function readDB() {
    try {
        const strData1 = await myQuery();
        const strData2 = await jsonData();
        return strData1, strData2

    } catch (error) {
        console.error(error);
    }
}
let results = readDB();

function jsonData() {
    return new Promise(function (resolve, reject) {
        fs.readFile('homework2_1.json', 'utf8', function (err, json) {
            if (err) {
                reject(err);
                return json
            }
            resolve(json);
        });
    });
}


router.get('/from_file', async ctx => {
    const result = await jsonData();
    ctx.body = result;
    console.log('sss')
})


app.use(serve(path.join(__dirname, 'public')));
app.use(router.routes());
app.use(router.allowedMethods());

app.listen(3008);
