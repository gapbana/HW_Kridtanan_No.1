const Koa = require('koa');
const Router = require('koa-router');
const serve = require('koa-static');
const path = require('path'); //import library path
const render = require('koa-ejs')  //import koa ejs
/* const mysql = require('mysql2/promise');  // get the client */
const mysql = require('mysql2/promise')



//สร้าง path ใหม่ = copy route

const app = new Koa();
const router = new Router();

render(app, {    //การ setting ejs
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
})

// create the connection
const pool = mysql.createPool({
    connectionLimit: 10,
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'hw12'
});


async function myQuery() {
    let [rows] = await pool.query('SELECT * FROM staff');
    return rows;
}

async function myQuery2() {
    let [rows] = await pool.query('SELECT * FROM book');  
    return rows;
}

async function readDB() {
    try {
        const strData = await myQuery();
        return strData

    } catch (error) {
        console.error(error);
    }
}

async function readDB2() {
    try {
        const strData = await myQuery2();
        return strData

    } catch (error) {
        console.error(error);
    }
}


router.get('/', async ctx => {
    const result = await readDB();
    const result2 = await readDB2();
    let obj = {};
    obj.staff = result;
    obj.book = result2;
    await ctx.render('table', obj)
    console.log(obj);
    console.log(typeof (obj));

})

console.log()
app.use(serve(path.join(__dirname, 'public')));
app.use(router.routes());
app.use(router.allowedMethods());

app.listen(3007);
