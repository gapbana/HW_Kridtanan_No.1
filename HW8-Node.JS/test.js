fs.writeFile('demofile2.txt', 'file2 content', 'utf8', writeFileCompleted);
function writeFileCompleted(err) {
  if (err) {
      console.error(err);               
      return;
  }
  fs.readFile('demofile2.txt', 'utf8', readFileCompleted);
}
function readFileCompleted(err, data) {
  if (err) {
      console.error(err);
      return;
  }
  console.log(data);
}
