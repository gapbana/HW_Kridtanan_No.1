
const Koa = require('koa');
const Router = require('koa-router');
const serve = require('koa-static');
const path = require('path'); //import library path
const render = require('koa-ejs')  //import koa ejs
//สร้าง path ใหม่ = copy route

const app = new Koa();
const router = new Router();

render(app, {                      //การ setting ejs
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
})


router.get('/index', ctx => {                   //เพิ่ม path contact
    ctx.body = 'this is contact page'
})
router.get('/contact', ctx => {                   //เพิ่ม path contact
    ctx.body = 'this is contact page'
})


/* router.get('/',ctx =>{          //เข้าหน้าหลักแล้วโหลดรูป
    ctx.body='<img src="images/jelly.jpg">'
}) */

router.get('/', async ctx => {
    await ctx.render('index')
})

router.get('/about', async ctx => {  //ห้ามลืม path
    await ctx.render('about')
})
router.get('/picturey',ctx =>{          //เข้าหน้าหลักแล้วโหลดรูป
    ctx.body='<img src="images/jelly.jpg">'
})




console.log()
app.use(serve(path.join(__dirname, 'public')));
app.use(router.routes());
app.use(router.allowedMethods());

app.listen(9999);
