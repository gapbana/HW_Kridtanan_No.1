const Koa = require('koa');
const Router = require('koa-router');
const serve = require('koa-static');
const path = require('path'); //import library path
const render = require('koa-ejs')  //import koa ejs
/* const mysql = require('mysql2/promise');  // get the client */
const mysql = require('mysql2/promise')



//สร้าง path ใหม่ = copy route

const app = new Koa();
const router = new Router();

render(app, {    //การ setting ejs
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
})

// create the connection
const pool = mysql.createPool({
    connectionLimit: 10,
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'hw13'
});


async function whoNoTeachQuery() {
    let [rows] = await pool.query('SELECT i.id,i.name,i.created_at FROM instructors as i LEFT JOIN courses as c on i.id = c.teach_by WHERE c.teach_by IS null');
    return rows;
}

async function noTeachInCourseQuery() {
    let [rows] = await pool.query('SELECT * FROM courses WHERE teach_by IS null');  
    return rows;
}

async function readDB() {
    try {
        const strData = await whoNoTeachQuery();
        return strData

    } catch (error) {
        console.error(error);
    }
}

 async function readDB2() {
    try {
        const strData = await noTeachInCourseQuery();
        return strData

    } catch (error) {
        console.error(error);
    }
} 


router.get('/', async ctx => {
    const result = await readDB();
    const result2 = await readDB2();
    let obj = {};
    obj.instructor = result;
    obj.courses = result2;
    await ctx.render('table', obj)
  console.log(obj);
    console.log(typeof (obj));

})

console.log()
app.use(serve(path.join(__dirname, 'public')));
app.use(router.routes());
app.use(router.allowedMethods());

app.listen(3010);
