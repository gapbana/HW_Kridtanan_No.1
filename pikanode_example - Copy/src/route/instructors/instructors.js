//แสดง JSON API ของ instuctor ทั้งตาราง
router.get('/instructor/find_all', async (ctx, next) => {
    const [rows] = await pool.query('SELECT * FROM `instructors`');
    ctx.body = rows;
    await next();
});

//รับค่า id เพื่อแสดง JSON API ของ instructor id ที่ระบุ จากตาราง instructors
router.get('/instructor/find_by_id/:id', async (ctx, next) => {
    const [rows] = await pool.query('SELECT * FROM `instructors` WHERE instructors.id = ?', [ctx.params.id]);
    ctx.body = rows;
    await next();
});

const find_by_id = async (ctx) => {
	const { email, password } = ctx.request.body
	// TODO: validate email, password
	const userId = await user.register(email, password)

	// TODO: handle user id ?
}