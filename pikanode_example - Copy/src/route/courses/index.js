const Router = require('koa-router')

const course = require('./course')
/* const signUp = require('./signup') */

const router = new Router()

router.get('/course/find_by_price/:price', courses.getHandler)
router.post('/signin', signIn.postHandler)
router.get('/signup', signUp.getHandler)
router.post('/signup', signUp.postHandler)

module.exports = router.routes()